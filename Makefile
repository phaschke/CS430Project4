all:
	gcc raytrace.c -o raytrace -lm

clean: 
	-rm -f raytrace
	-rm output.ppm
