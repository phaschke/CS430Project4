/*
 * Peter Haschke
 * CS430: Project 4 - Recursive Raytracing
 * 11/15/16
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define MAX_COLOR_VAL 255
#define MAX_LEVEL 7

#define CAMERA_TYPE 0
#define LIGHT_TYPE 1
#define SPHERE_TYPE 2
#define PLANE_TYPE 3

#define POINT_LIGHT 0
#define SPOT_LIGHT 1

//Polymorphism in C
//Structs for camera, sphere, plane
typedef struct{
        int type; //0 for camera, 1 for sphere, 2 for plane.
        union{
                struct{
                        double width;
                        double height;
                } camera;
                struct{
                        double diffuseColor[3];
                        double specularColor[3];
                        double position[3];
                        double radius;
                        double reflectivity;
                        double refractivity;
                        double ior;
                } sphere;
                struct{
                        double diffuseColor[3];
                        double specularColor[3];
                        double position[3];
                        double normal[3];
                        double reflectivity;
                        double refractivity;
                        double ior;
                } plane;
                struct{
                        int lightType; //0 for point, 1 for spotlight 
                        double color[3];
                        double position[3];
                        double direction[3];
                        double theta;
                        double radialA2;
                        double radialA1;
                        double radialA0;
                        double angularA0;
               } light;
        };
} Object;

typedef struct pixelStruct{
        unsigned char r, g, b;
} Pixel;

//Function Prototypes:
int nextChar(FILE* fd);
void expectChar(FILE* fd, int d);
void skipWS(FILE* fd);
char* nextString(FILE* fd);
double nextNumber(FILE* fd);
double* nextVector(FILE* fd);
void readScene(char* fileName, Object** objects);
int raycast(Object** objects, Pixel *pixelBuffer, int width, int height);
void writeHeader(FILE* fdOut, int width, int height);
double* findCamera(Object** objects);
double sphereIntersection(double* Ro, double* Rd, double* position, double radius);
double planeIntersection(double* Ro, double* Rd, double* position, double* normal);
void printPixelBuffer(FILE* fdOut, Pixel *pixelBuffer, int numPixels);
double clamp(double colorVal);
static inline double sqr(double v);
static inline void normalize(double* v);
static inline double dot(double* u, double* v);
static inline void add(double* u, double* v, double* w);
static inline void subtract(double* u, double* v, double* w);
static inline void scale(double* u, double s, double* v);
static inline void cross(double* u, double* v, double* w);
void printStruct(Object** objects);
static inline double degToRad(double d);
static inline void multiply(double* u, double* v, double* w);
void shoot(Object** objects, Object* skipObject, Object** closestObjectPointer, double* closestTPointer, double* Ro, double* Rd);
void  shade(Object** objects, Object* closestObject, double* color, double* Rd, double* RoNew, int level);
void directShade(double* tempColor, Object* closestObject, Object* IllumObject, double* Rd, double* RoNew, double* RdNew, double distanceToLight);
void reflectionVector(double* R, double* RoNew, Object* closestObject, double* Rd);
void refractionVector(double* refracRd, double* refracRoOut, Object* closestObject, double* RoNew, double* Rd);
void bendRay(double* Ut, double* N, double* Rd, double iorInc, double iorTrans);


//Helper function for adding vectors
static inline void add(double* u, double* v, double* w){
        w[0] = u[0] + v[0];
        w[1] = u[1] + v[1];
        w[2] = u[2] + v[2];
}

//Helper function for adding vectors
static inline void multiply(double* u, double* v, double* w){
        w[0] = u[0] * v[0];
        w[1] = u[1] * v[1];
        w[2] = u[2] * v[2];
}


//Helper function for subtracting vectors
static inline void subtract(double* u, double* v, double* w){
        w[0] = u[0] - v[0];
        w[1] = u[1] - v[1];
        w[2] = u[2] - v[2];
}

//Helper function for scaling vectors
static inline void scale(double* u, double s, double* v){
        v[0] = s * u[0];
        v[1] = s * u[1];
        v[2] = s * u[2];
}

//Cross product
static inline void cross(double* u, double* v, double* w){
        w[0] = u[1]*v[2] - u[2]*v[1];
        w[1] = u[2]*v[0] - u[0]*v[2];
        w[2] = u[0]*v[1] - u[1]*v[0];
}


//Helper function to square a number.
static inline double sqr(double v){
        return v*v;
}

//Helper function to normalize a ray
static inline void normalize(double* v){
        double len = sqrt(sqr(v[0]) + sqr(v[1]) + sqr(v[2]));
        v[0] /= len;
        v[1] /= len;
        v[2] /= len;
}

static inline double dot(double* u, double* v) {
          return u[0]*v[0] + u[1]*v[1] + u[2]*v[2];
}

static inline double degToRad(double d){
        return d*(M_PI/180.0);
}

//Global variable line stores the current line number for error reporting.
int line = 1;

int main(int c, char** argv) {
        if(c != 5){
                fprintf(stderr, "Error: not enough arguments, expected: raycast width height input.json output.ppm\n");
                exit(1);
        }

        int width = atoi(argv[1]);
        if(width == 0){
                fprintf(stderr, "Error: Width must be greater than 0.\n");
                exit(1);
        }

        int height = atoi(argv[2]);
        if(height == 0){
                fprintf(stderr, "Error: Height must be bigger than 0.\n");
                exit(1);
        }

        int numPixels = width*height;

        //Allocate memory for all objects in the file.
        Object** objects;
        objects = malloc(sizeof(Object*)*128);
        
        //Allocate memory for pixel information.
        Pixel *pixelBuffer;
        pixelBuffer = (Pixel *)malloc(sizeof(Pixel)*width*height);
        
        //Parse json to get all objects into the struct array.
        readScene(argv[3], objects);

        printf("Read scene into memory from JSON file.\n");

        //printStruct(objects);
        //Do the raycasting and write the data to the file.
        raycast(objects, pixelBuffer, width, height);

        printf("Perform raycast.\n");

        FILE* fdOut;
        fdOut = fopen(argv[4], "w");
        if(fdOut == NULL){
                fprintf(stderr, "Problem opening %s for writing.\n", argv[4]);
                exit(1);
        }

        //Write the ppm file header.
        writeHeader(fdOut, width, height);

        //Write the pixel buffer.
        printPixelBuffer(fdOut, pixelBuffer, numPixels);

        printf("Write pixels to image\n");

        //Free alocated memory
        int i = 0;
        while(objects[i] != NULL){
                free(objects[i]);
                i++;
        }
        free(pixelBuffer);
        free(objects);

        return 0;
}

int raycast(Object** objects, Pixel *pixelBuffer, int width, int height){

        double* v = findCamera(objects);
        double cameraWidth = v[0];
        double cameraHeight = v[1];

        double pixHeight = cameraHeight/height;
        double pixWidth = cameraWidth/width;
        double cx = 0;
        double cy = 0;
   
        double *color = malloc(sizeof(double)*3); //Allocate space for color.
        color[0] = 0.0;
        color[1] = 0.0;
        color[2] = 0.0;

        int y = 0;
        int x = 0;
        for(y=0; y<height; y++){
                for(x=0; x<width; x++){
                        double Ro[3] = {0, 0, 0};
                        //Rd normilize(P - Ro)
                        double Rd[3] = {
                                cx - (cameraWidth/2) + pixWidth * (x + 0.5),
                                cy + (cameraHeight/2) - pixHeight * (y + 0.5),
                                1
                        };
                        normalize(Rd);
                
                        Object* closestObject;
                        double closestT;

                        shoot(objects, NULL, &closestObject, &closestT, Ro, Rd);

                        if(closestObject != NULL){ //If there was an object in front of the camera.
                                //Calculate new Ro  and Dd
                                double RoNew[3];
                                scale(Rd, closestT, RoNew);
                                add(RoNew, Ro, RoNew);
                                
                                shade(objects, closestObject, color,  Rd, RoNew, 0);

                                //We have calculated the color.
                                //Print color to pixel struct
                                pixelBuffer->r = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[0]));
                                pixelBuffer->g = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[1]));
                                pixelBuffer->b = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[2]));

                        } else {
                                color[0] = 0.0;
                                color[1] = 0.0;
                                color[2] = 0.0;

                                pixelBuffer->r = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[0]));
                                pixelBuffer->g = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[1]));
                                pixelBuffer->b = (unsigned char)((double)MAX_COLOR_VAL * clamp(color[2]));
                        }
                        *pixelBuffer++;
                } //End for on width
        } //End for on height
        free(color);
        return 0;
}

void shade(Object** objects, Object* closestObject, double* color, double* Rd, double* RoNew, int level){
        color[0] = 0.0;
        color[1] = 0.0;
        color[2] = 0.0;
        
        if(level <= MAX_LEVEL){

                double refRd[3], refracRd[3], RoNewEps[3], refracRoOut[3], refracRoNew[3], IR[3], IT[3], refRoNew[3], directColor[3], IL[3], Kr, Kt;

                reflectionVector(refRd, RoNew, closestObject, Rd);
                scale(refRd, .01, RoNewEps);
                add(RoNewEps, RoNew, RoNewEps);

                Object* nextObject;
                double nextObjectT;

                shoot(objects, NULL, &nextObject, &nextObjectT, RoNewEps, refRd);

                if(nextObject == NULL){
                        IR[0] = 0.0;
                        IR[1] = 0.0;
                        IR[2] = 0.0;
                } else {
                        //RoNew + nextObjectT * refVec
                        scale(refRd, nextObjectT, refRoNew);
                        add(refRoNew, RoNewEps, refRoNew);
                        shade(objects, nextObject, IR, refRd, refRoNew, level+1);
                }

                refractionVector(refracRd, refracRoOut, closestObject, RoNew, Rd);

                scale(refracRd, .01, RoNewEps);
                add(refracRoOut, RoNewEps, refracRoOut);

                shoot(objects, NULL, &nextObject, &nextObjectT, refracRoOut, refracRd);

                if(nextObject == NULL){
                        IT[0] = 0.0;
                        IT[1] = 0.0;
                        IT[2] = 0.0;
                } else {
                        //RoNew + nextObjectT * refVec
                        scale(refracRd, nextObjectT, refracRoNew);
                        add(refracRoNew, refracRoOut, refracRoNew);

                        shade(objects, nextObject, IT, refracRd, refracRoNew, level+1);

                }

                //Iterate over all lights.
                IL[0] = 0.0;
                IL[1] = 0.0;
                IL[2] = 0.0;
                int i = 0;
                while(objects[i] != NULL){
                        if(objects[i]->type == LIGHT_TYPE){
                                //Do shadow test

                                double RdNew[3];
                                subtract(objects[i]->light.position, RoNew, RdNew);
                                double distanceToLight = sqrt(dot(RdNew, RdNew));
                                normalize(RdNew);

                                Object* closestShadowObject;
                                double closestShadowT;

                                shoot(objects, closestObject, &closestShadowObject, &closestShadowT, RoNew, RdNew);

                                if(closestShadowT > distanceToLight){

                                        if(closestShadowObject == NULL){ //We are not in a shadow.

                                                directShade(directColor, closestObject, objects[i], Rd, RoNew, RdNew, distanceToLight);

                                                add(IL, directColor, IL);
                                        
                                        } //If shadow object is null
                                } //If shadowT > distanceToLight
                        } //end if light type 
                i++;
                } //End while to iterate over all lights
        if(closestObject->type == SPHERE_TYPE){
                Kr = closestObject->sphere.reflectivity;
                Kt = closestObject->sphere.refractivity;
        }else if(closestObject->type == PLANE_TYPE){
                Kr = closestObject->plane.reflectivity;
                Kt = closestObject->plane.refractivity;
        }else{
                fprintf(stderr, "Error: Invalid object type in Kr and Kt setting.\n");
        }
                scale(IL, 1-Kr-Kt, color);
                scale(IR, Kr, IR);
                scale(IT, Kt, IT);
                add(color, IR, color);
                add(color, IT, color);
        
        } //End if level <= MAX_LEVEL
}

void bendRay(double* Ut, double* N, double* Rd, double iorInc, double iorTrans){
        double a[3], b[3], minCosPhi, sinPhi, sinTheta, tempVec[3];

        //a is the normal to the plane working in.
        cross(N, Rd, a);
        normalize(a);
        //Calculate surface tangent in b.
        cross(a, N, b);

        sinTheta = dot(Rd, b);

        sinPhi = (iorInc/iorTrans)*sinTheta;

        minCosPhi = -1.0*sqrt(1.0 - clamp(sinPhi*sinPhi));

        //Calculate trasmit vector.
        scale(N, minCosPhi, Ut);
        scale(b, sinPhi, tempVec);
        add(Ut, tempVec, Ut);
}

void refractionVector(double* refracRd, double* refracRoOut, Object* closestObject, double* RoNew, double* Rd){
        double N[3], Ut[3], closestT, Eps, RoNewEps[3];

        if(closestObject->type == SPHERE_TYPE){
                subtract(RoNew, closestObject->sphere.position, N);
                normalize(N);

                //Refraction on ray entrance.
                bendRay(Ut, N, Rd, 1.0, closestObject->sphere.ior);

                //Add epsilon value to make sure we are actually in the sphere.
                Eps = 0.001;
                scale(Rd, Eps, RoNewEps);
                add(RoNewEps, RoNew, RoNewEps);
                
                closestT = sphereIntersection(RoNewEps, Ut, closestObject->sphere.position, closestObject->sphere.radius);
                if(closestT == -1){
                        //Assume ray is tangent to sphere.
                        refracRd = Rd;
                        refracRoOut = RoNew;
                }else {
                        //Compute location of ray exit on sphere.
                        scale(Ut, closestT, refracRoOut);
                        add(refracRoOut, RoNewEps, refracRoOut);

                        //Compute normal at refracted ray exit.
                        subtract(refracRoOut, closestObject->sphere.position, N);
                        normalize(N);
                        
                        //Refraction on exit.
                        bendRay(refracRd, N, Ut, closestObject->sphere.ior, 1.0);
                }

        }else if(closestObject->type == PLANE_TYPE){
                //No thickness in plane for refeaction through medium.
                refracRd = Rd;
                refracRoOut = RoNew;
        }else{
                fprintf(stderr, "Error: Invalid object type in N  setting.\n");
        }

}

void reflectionVector(double* R, double* RoNew, Object* closestObject, double* Rd){
        double N[3];
        if(closestObject->type == SPHERE_TYPE){
                subtract(RoNew, closestObject->sphere.position, N);
                normalize(N);
        }else if(closestObject->type == PLANE_TYPE){
                memcpy(N, closestObject->plane.normal, sizeof(closestObject->plane.normal));
                normalize(N);
        }else{
                fprintf(stderr, "Error: Invalid object type in N  setting.\n");
        }
        //R = Rd-2n(Rd*N)N
        scale(N, 2.0*dot(N, Rd), R);
        subtract(Rd, R, R);


}

//IllumObject = objects[i]
void directShade(double* directColor, Object* closestObject, Object* IllumObject, double* Rd, double* RoNew, double* RdNew, double distanceToLight){
        //Calculate diffuse, specular
        
        double Kd[3];
        double Ks[3];

        //N = object->normal is plane, and newRo - object->center
        double N[3];
        if(closestObject->type == SPHERE_TYPE){
                subtract(RoNew, closestObject->sphere.position, N);
                normalize(N);
                memcpy(Kd, closestObject->sphere.diffuseColor, sizeof(closestObject->sphere.diffuseColor));
                memcpy(Ks, closestObject->sphere.specularColor, sizeof(closestObject->sphere.specularColor));
        }else if(closestObject->type == PLANE_TYPE){
                memcpy(N, closestObject->plane.normal, sizeof(closestObject->plane.normal));
                normalize(N);
                memcpy(Kd, closestObject->plane.diffuseColor, sizeof(closestObject->plane.diffuseColor));
                memcpy(Ks, closestObject->plane.specularColor, sizeof(closestObject->plane.specularColor));
        }else{
                fprintf(stderr, "Error: Invalid object type in N, Kd, Ks setting.\n");
        }

        double Il[3];
        memcpy(Il, IllumObject->light.color, sizeof(IllumObject->light.color));
       
        //Calculate diffuse
        double Ia[3] = {0.0, 0.0, 0.0}; //Set illumination ambient value

        double diffuse[3] = {0.0, 0.0, 0.0};

        if(dot(N, RdNew) > 0.0){
                multiply(Kd, Il, diffuse);
                scale(diffuse, dot(N, RdNew), diffuse);
        }

        //V = -Rd
        double V[3];
        scale(Rd, -1.0, V);
        
        //L = RdNew (light_position - RoNew)
        
        //R = (2N dot L)N - L        
        double R[3];
        scale(N, 2.0*dot(N, RdNew), R);
        subtract(R, RdNew, R);
        
        //Calculate specular
        double Ns = 20;
        double specular[3] = {0.0, 0.0, 0.0};
        if(dot(V, R) > 0 && dot(N, RdNew) > 0){
                
                multiply(Ks, Il, specular);
                scale(specular, pow((dot(V, R)), Ns), specular);
        }
        double radialAttenuation;
        double angularAttenuation = 0.0;

        //Calculate the attenuations
        if(IllumObject->light.lightType == POINT_LIGHT){
                
                radialAttenuation = 1/((IllumObject->light.radialA2 * sqr(distanceToLight) + (IllumObject->light.radialA1 * distanceToLight) + IllumObject->light.radialA0));
                angularAttenuation = 1.0; //If not a spot light angular is = 1.
        }else if(IllumObject->light.lightType == SPOT_LIGHT){
                //Compute V0 and Vl
                double theta = IllumObject->light.theta;
                
                //Vlight
                double Vl[3];
                memcpy(Vl, IllumObject->light.direction, sizeof(IllumObject->light.direction));
                normalize(Vl);
                
                //Vobject
                double Vo[3];
                //scale(RdNew, -1.0, Vo);
                subtract(RoNew, IllumObject->light.position, Vo);
                normalize(Vo);
                
                //Check
                if(dot(Vo, Vl) > cos(degToRad(theta))){
                        angularAttenuation = pow((dot(Vo, Vl)), IllumObject->light.angularA0);
                        radialAttenuation = 1;
                }
        }else{
                fprintf(stderr, "Error: Invalid light type!\n");
        }

        add(diffuse, specular, directColor);
        scale(directColor, (radialAttenuation * angularAttenuation), directColor);

}

void shoot(Object** objects, Object* skipObject, Object** closestObjectPointer, double* closestTPointer, double* Ro, double* Rd){

        Object* closestObject = NULL;
        double closestT = INFINITY;

        int i = 0;
        //Iterate over every object again for shadow test.
        while (objects[i] != NULL){
                double tempClosestT = 0;
                Object* tempObject;
                if(objects[i] == skipObject){ //Skip over itself.
                        i++;
                        continue;
                }
                switch(objects[i]->type){
                        case CAMERA_TYPE:
                                //Camera case, do nothing.
                        break;
                        case LIGHT_TYPE:
                                //Light case, do nothing.
                        break;
                        case SPHERE_TYPE:
                                tempObject = objects[i]; 
                                tempClosestT = sphereIntersection(Ro, Rd, objects[i]->sphere.position, objects[i]->sphere.radius);
                        break;
                        case PLANE_TYPE:
                                tempObject = objects[i];
                                tempClosestT = planeIntersection(Ro, Rd, objects[i]->plane.position, objects[i]->plane.normal);
                        break;
                        default:
                                fprintf(stderr, "Not a valid object type! In shadow test.\n");
                                exit(1);
                } //End switch on object type
                
                //Handle overlapping. Use closer object.
                if(tempClosestT > 0 && tempClosestT < closestT){
                        closestT = tempClosestT;
                        closestObject = tempObject;
                }

        i++;

        } //End while interating over all objects.

        *closestObjectPointer = closestObject;
        *closestTPointer = closestT;
}

double sphereIntersection(double* Ro, double* Rd, double* position, double radius){
        double a = sqr(Rd[0]) + sqr(Rd[1]) + sqr(Rd[2]);
        double b = 2 * Rd[0] * (Ro[0]-position[0]) + 2 * Rd[1] * (Ro[1]-position[1]) + 2 * Rd[2] * (Ro[2]-position[2]);
        double c = sqr(position[0]) + sqr(position[1]) + sqr(position[2]) + sqr(Ro[0]) + sqr(Ro[1]) + sqr(Ro[2]) +
                -2*(Ro[0]*position[0] + Ro[1]*position[1] + Ro[2]*position[2]) - sqr(radius);

        double det = sqr(b) - 4 * a * c;
        //If det is less than 0 is does not intersect, else it does.
        if(det < 0){
                //Does not intersect
                return -1;
        }

        det = sqrt(det);

        // (-b - sqrt(-4ac))/2a
        double t0 = (-b - det) / (2 * a);
        if (t0 > 0){
                return t0;
        }

        // (-b + sqrt(-4ac))/2a
        double t1 = (-b + det) / (2 * a);
        if(t1 > 0){
                return t1;
        }

        return -1;
}

double planeIntersection(double* Ro, double* Rd, double* position, double* normal){
        
        double Vd = dot(normal, Rd);

        if(fabs(Vd) < 1.0E-10){
                return -1;
        }
        
        double d = -(dot(position, normal));
        double t = -(dot(normal, Ro) + d) / (Vd);
        
        return t;
}

double clamp(double colorVal){
        if(colorVal >= 1){
                return 1.0;
        }
        if(colorVal <= 0){
                return 0.0;      
        }
        return colorVal;
}

//Function to print the pixel buffer ro the output file.
void printPixelBuffer(FILE* fdOut, Pixel *pixelBuffer, int numPixels){
        int i, pixelCount;

        for(i = 0; i < numPixels; i++){
                fwrite(&pixelBuffer->r, 1, 1, fdOut);
                fwrite(&pixelBuffer->g, 1, 1, fdOut);
                fwrite(&pixelBuffer->b, 1, 1, fdOut);
        
                *pixelBuffer++;
                pixelCount++;
        }
        return;
}

//Function to write the header of the ppm file to the output file.
void writeHeader(FILE* fdOut, int width, int height){
        if(fprintf(fdOut, "%c%d%c", 'P', 6, 10) == 0){
                fprintf(stderr, "Error: writing magic number to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", width, 10) == 0){
                fprintf(stderr, "Error: writing width to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", height, 10) == 0){
                fprintf(stderr, "Error: writing height to header.\n");
                fclose(fdOut);
                exit(1);
        }
        if(fprintf(fdOut, "%d%c", MAX_COLOR_VAL, 10) == 0){
                fprintf(stderr, "Error: writing max color value to header.\n");
                fclose(fdOut);
                exit(1);
        }

}

//Function to find the camera object and return the width and height.
//If no camera object is found return error.
double* findCamera(Object** objects){
        int i = 0;
        while (objects[i] != NULL){
                if(objects[i]->type == 0){
                        double* v = malloc(sizeof(double)*2);
                        v[0] = objects[i]->camera.width;
                        v[1] = objects[i]->camera.height;
                        return v;
                }
                i++;
        }
        fprintf(stderr, "Error: No camera objects found in input file.\n");
        exit(1);
}

void readScene(char* filename, Object** objects) {
        int c = 0;
        FILE* fdIn = fopen(filename, "r");

        if (fdIn == NULL) {
                fprintf(stderr, "Error: Could not open file \"%s\"\n", filename);
                exit(1);
        }
  
         skipWS(fdIn);
  
        // Find the beginning of the list
        expectChar(fdIn, '[');
        skipWS(fdIn);

        // Find the objects
        int i = 0;
        while (1) {
                c = fgetc(fdIn);


                if (c == '{') {
                        skipWS(fdIn);
            
                        // Parse the object
                        char* key = nextString(fdIn);
                        if (strcmp(key, "type") != 0) {
                                fprintf(stderr, "Error: Expected \"type\" key on line number %d.\n", line);
                                exit(1);
                        }

                        skipWS(fdIn);

                        expectChar(fdIn, ':');

                        skipWS(fdIn);

                        char* value = nextString(fdIn);
                        
                        int type;
                        if (strcmp(value, "camera") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = CAMERA_TYPE;
                                type = CAMERA_TYPE;
                        } else if (strcmp(value, "light") == 0){
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = LIGHT_TYPE;

                                //Set default light value to point light, if there is a direction update type to spot light.
                                objects[i]->light.lightType = POINT_LIGHT;

                                //Set default values for radial and angular attuniations:
                                objects[i]->light.radialA2 = 1;
                                objects[i]->light.radialA1 = 0;
                                objects[i]->light.radialA0 = 0;
                                objects[i]->light.angularA0 = 0;

                                type = LIGHT_TYPE;
                        } else if (strcmp(value, "sphere") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = SPHERE_TYPE;

                                objects[i]->sphere.reflectivity = 0;
                                objects[i]->sphere.reflectivity = 0;
                                objects[i]->sphere.ior = 1;

                                type = SPHERE_TYPE;
                        } else if (strcmp(value, "plane") == 0) {
                                objects[i] = malloc(sizeof(Object));
                                objects[i]->type = PLANE_TYPE;

                                objects[i]->plane.reflectivity = 0;
                                objects[i]->plane.reflectivity = 0;
                                objects[i]->plane.ior = 1;

                                type = PLANE_TYPE;
                        } else {
                                fprintf(stderr, "Error: Unknown type, \"%s\", on line number %d.\n", value, line);
                                exit(1);
                        }

                        skipWS(fdIn);

                        while (1) {
                                // , }
                                c = nextChar(fdIn);
                                if (c == '}') {
                                         // stop parsing this object
                                        break;
                                } else if (c == ',') {
                                        // read another field
                                        skipWS(fdIn);
                                        char* key = nextString(fdIn);
                                        skipWS(fdIn);
                                        expectChar(fdIn, ':');
                                        skipWS(fdIn);
                                        //Get width (only camera)
                                        if (strcmp(key, "width") == 0){
                                                objects[i]->camera.width = nextNumber(fdIn);
                                        //Get height (only camera)
                                        }else if (strcmp(key, "height") == 0){
                                                objects[i]->camera.height = nextNumber(fdIn);
                                        //Get radius (only sphere)
                                        }else if (strcmp(key, "radius") == 0) {
                                                objects[i]->sphere.radius = nextNumber(fdIn);
                                        //Get direction (only spotlights)
                                        }else if (strcmp(key, "direction") == 0){
                                                memcpy(objects[i]->light.direction, nextVector(fdIn), sizeof(objects[i]->light.direction));
                                                //We know it is a spot light, set light type
                                                objects[i]->light.lightType = SPOT_LIGHT;
                                        //Get radials and angular
                                        }else if (strcmp(key, "radial-a2") == 0){
                                                objects[i]->light.radialA2 = nextNumber(fdIn);
                                        }else if (strcmp(key, "radial-a1") == 0){
                                                objects[i]->light.radialA1 = nextNumber(fdIn); 
                                        }else if (strcmp(key, "radial-a0") == 0){
                                                objects[i]->light.radialA0 = nextNumber(fdIn); 
                                        }else if (strcmp(key, "angular-a0") == 0){
                                                objects[i]->light.angularA0 = nextNumber(fdIn); 
                                        //Get theta
                                        }else if (strcmp(key, "theta") == 0){
                                                objects[i]->light.theta = nextNumber(fdIn);
                                        //Get reflectivity
                                        }else if (strcmp(key, "reflectivity") == 0){
                                                if(type == SPHERE_TYPE){
                                                        objects[i]->sphere.reflectivity = nextNumber(fdIn);
                                                }
                                                if(type == PLANE_TYPE){
                                                       objects[i]->plane.reflectivity = nextNumber(fdIn); 
                                                }
                                        //Get refractivity
                                        }else if (strcmp(key, "refractivity") == 0){
                                                if(type == SPHERE_TYPE){
                                                        objects[i]->sphere.refractivity = nextNumber(fdIn);
                                                }
                                                if(type == PLANE_TYPE){
                                                       objects[i]->plane.refractivity = nextNumber(fdIn); 
                                                }
                                        //Get ior
                                        }else if (strcmp(key, "ior") == 0){
                                                if(type == SPHERE_TYPE){
                                                        objects[i]->sphere.ior = nextNumber(fdIn);
                                                }
                                                if(type == PLANE_TYPE){
                                                       objects[i]->plane.ior = nextNumber(fdIn); 
                                                }
                                        //Get color
                                        }else if (strcmp(key, "color") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == LIGHT_TYPE){
                                                        memcpy(objects[i]->light.color, value, sizeof(objects[i]->light.color));
                                                }
                                                free(value);
                                        }else if(strcmp(key, "diffuse_color") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == SPHERE_TYPE){
                                                        memcpy(objects[i]->sphere.diffuseColor, value, sizeof(objects[i]->sphere.diffuseColor));
                                                }
                                                if(type == PLANE_TYPE){
                                                        memcpy(objects[i]->plane.diffuseColor, value, sizeof(objects[i]->plane.diffuseColor));
                                                }
                                                free(value);
                                        //Get position
                                        }else if(strcmp(key, "specular_color") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == SPHERE_TYPE){
                                                        memcpy(objects[i]->sphere.specularColor, value, sizeof(objects[i]->sphere.diffuseColor));
                                                }
                                                if(type == PLANE_TYPE){
                                                        memcpy(objects[i]->plane.specularColor, value, sizeof(objects[i]->plane.diffuseColor));
                                                }
                                                free(value);

                                        }else if (strcmp(key, "position") == 0){
                                                double* value = nextVector(fdIn);
                                                if(type == LIGHT_TYPE){
                                                        memcpy(objects[i]->light.position, value, sizeof(objects[i]->light.position));
                                                }
                                                if(type == SPHERE_TYPE){
                                                        memcpy(objects[i]->sphere.position, value, sizeof(objects[i]->sphere.position));
                                                }
                                                if(type == PLANE_TYPE){
                                                        memcpy(objects[i]->plane.position, value, sizeof(objects[i]->plane.position));
                                                }
                                                free(value);
                                        //Get normal (only plane)
                                        }else if (strcmp(key, "normal") == 0) {
                                                double* value = nextVector(fdIn);
                                                memcpy(objects[i]->plane.normal, value, sizeof(objects[i]->plane.normal));
                                                free(value);
                                        //Unknown property
                                        }else{
                                                fprintf(stderr, "Error: Unknown property, \"%s\", on line %d.\n", key, line);
                                                //char* value = nextString(fdIn);
                                        }
                                        skipWS(fdIn);
                                } else {
                                        fprintf(stderr, "Error: Unexpected value on line %d\n", line);
                                exit(1);
                                }
                        } //End inner while
                        skipWS(fdIn);
                        c = nextChar(fdIn);
                        if (c == ',') {
                                // noop
                                skipWS(fdIn);
                        } else if (c == ']') {
                                objects[i+1] = NULL; //Null terminate object array.
                                fclose(fdIn);
                                return;
                        } else {
                                fprintf(stderr, "Error: Expecting ',' or ']' on line %d.\n", line);
                                exit(1);
                        }
                }else if (c == ']') { //end if '{'
                        //Hit end of file.
                        //objects[i] = NULL; //Null terminate object array.
                        if(i == 0){
                                fprintf(stderr, "Worst scene file EVER!, nothing in it!\n");
                                fclose(fdIn);
                                exit(1);
                        }
                        objects[i+1] = NULL; 
                        fclose(fdIn);
                        break;
                }else{
                        fprintf(stderr, "Error: Expecting '}' or ']' character on line %d.\n", line);
                        fclose(fdIn);
                        exit(1);
                }

                i++; //iterate position in object struct array.
        } //end outer while
} //end function



// nextChar() wraps the getc() function and provides error checking and line
// number maintenance
int nextChar(FILE* fdIn) {
         int c = fgetc(fdIn);
        #ifdef DEBUG
        printf("nextChar: '%c'\n", c);
        #endif
        if (c == '\n') {
                line += 1;
        }
        if (c == EOF) {
                fprintf(stderr, "Error: Unexpected end of file on line number %d.\n", line);
        exit(1);
        }
return c;
}

// expectChar() checks that the next character is d.  If it is not it emits
// an error.
void expectChar(FILE* fdIn, int d) {
        int c = nextChar(fdIn);
        if (c == d) return;
        fprintf(stderr, "Error: Expected '%c' on line %d.\n", d, line);
        exit(1);    
}

// skipWS() skips white space in the file.
void skipWS(FILE* fdIn) {
        int c = nextChar(fdIn);
        while (isspace(c)) {
                c = nextChar(fdIn);
        }
        ungetc(c, fdIn);
}

// nextString() gets the next string from the file handle and emits an error
// if a string can not be obtained.
char* nextString(FILE* fdIn) {
  char buffer[129];
  int c = nextChar(fdIn);
  if (c != '"') {
    fprintf(stderr, "Error: Expected string on line %d.\n", line);
    exit(1);
  }  
  c = nextChar(fdIn);
  int i = 0;
  while (c != '"') {
    if (i >= 128) {
      fprintf(stderr, "Error: Strings longer than 128 characters in length are not supported.\n");
      exit(1);      
    }
    if (c == '\\') {
      fprintf(stderr, "Error: Strings with escape codes are not supported.\n");
      exit(1);      
    }
    if (c < 32 || c > 126) {
      fprintf(stderr, "Error: Strings may contain only ascii characters.\n");
      exit(1);
    }
    buffer[i] = c;
    i += 1;
    c = nextChar(fdIn);
  }
  buffer[i] = 0;
  return strdup(buffer);
}

double nextNumber(FILE* fdIn) {
        double value;
        int read = fscanf(fdIn, "%lf", &value);
        if(read == 0){
                fprintf(stderr, "Failed to read number in on line %d.\n", line);
        }
        return value;
}

double* nextVector(FILE* fdIn) {
        double* v = malloc(3*sizeof(double));
        expectChar(fdIn, '[');
        skipWS(fdIn);
        v[0] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ',');
        skipWS(fdIn);
        v[1] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ',');
        skipWS(fdIn);
        v[2] = nextNumber(fdIn);
        skipWS(fdIn);
        expectChar(fdIn, ']');
        return v;
}

void printStruct(Object** objects){
        printf("Printing all objects in object struct.\n");
        int i =0;
        while(objects[i] != NULL){
                if(objects[i]->type == CAMERA_TYPE){
                        
                        printf("Type: %d, Width: %lf, Height: %lf\n", objects[i]->type, objects[i]->camera.width, objects[i]->camera.height);
                }
                if(objects[i]->type == PLANE_TYPE){
                        printf("Type: %d, Normal: [%lf, %lf, %lf], Position: [%lf, %lf, %lf], Diffuse: [%lf, %lf, %lf].\n", objects[i]->type, objects[i]->plane.normal[0], objects[i]->plane.normal[1], objects[i]->plane.normal[2], objects[i]->plane.position[0], objects[i]->plane.position[1], objects[i]->plane.position[2], objects[i]->plane.diffuseColor[0], objects[i]->plane.diffuseColor[1], objects[i]->plane.diffuseColor[2]);
                }
                i++;

        }
}
